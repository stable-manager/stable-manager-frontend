# Frontend part of Stable Manager application

## General information 

This is the frontend part of the Stable Manager application. I think the most important thing for any user is an intuitive menu, so I created a very simple and elegant user interface.
If you want to check the application before downloading and using it by yourself, you can see a short demo below. 

![](demo-gif.gif)


## Getting started 
 
 Remember to run the backend part of Stable Manager! 
1. Install dependancies 
```sh
npm install
```
2. Run application 
```sh
ng serve
```
3. Open your browser on :
```sh
 http://localhost:4200/
```
