import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Rider } from './rider';
import { StableManagerService } from './stable-manager.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  public riders!: Rider[];
  public editRider!: Rider;
  public deleteRider!: Rider;
  title: "stablemanagerapplication";

  constructor(private stableManagerService: StableManagerService){
  }

  ngOnInit() {
    this.getRiders();
  }

  public getRiders() : void {
    this.stableManagerService.getRiders().subscribe(
      (response: Rider[]) => {
        this.riders = response;
      },
      (error : HttpErrorResponse) => {
          alert(error.message);
      }
    )
  }

  public onAddRider(addForm: NgForm) : void {
    document.getElementById('add-rider-form')?.click();
    this.stableManagerService.addRider(addForm.value).subscribe(
      (response: Rider) => {
        console.log(response);
        this.getRiders();
        addForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
      },
    )
  }

  public onUpdateRider(rider: Rider) : void {

    this.stableManagerService.updateRider(rider).subscribe(
      (response: Rider) => {
        console.log(response);
        this.getRiders();
        rider === null;

      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      },
    )
  }

  public onDeleteRider(riderId: number): void {
    this.stableManagerService.deleteRider(riderId).subscribe(
      (response: void) => {
        console.log(response);
        this.getRiders();

      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public searchRider(key: string): void {
    console.log(key);
    const results: Rider[] = [];
    for (const rider of this.riders) {
      if (rider.fullName.toLowerCase().indexOf(key.toLowerCase()) !== -1
      || rider.email.toLowerCase().indexOf(key.toLowerCase()) !== -1
      || rider.phoneNumber.toLowerCase().indexOf(key.toLowerCase()) !== -1
      || rider.ridingSkills.toLowerCase().indexOf(key.toLowerCase()) !== -1) {
        results.push(rider);
      }
    }
    this.riders = results;
    if (results.length === 0 || !key) {
      this.getRiders();
    }
  }

  public onOpenModal(rider : Rider, mode: string) : void {
    const container = document.getElementById('main-container')
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    if(mode === 'add'){
      button.setAttribute('data-target', '#addRiderModal');
    }
    if(mode === 'edit'){
      this.editRider = rider;
      button.setAttribute('data-target', '#updateRiderModal');

    }
    if(mode === 'delete'){
      this.deleteRider = rider;
      button.setAttribute('data-target', '#deleteRiderModal');
    }

    container?.appendChild(button);
    button.click();
  }
}
