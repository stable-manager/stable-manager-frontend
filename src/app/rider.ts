export interface Rider {
    id: number;
    fullName: string; 
    email: string;
    ridingSkills: string; 
    phoneNumber: string;
    imageUrl: string;

}