import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Rider } from './rider';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StableManagerService {
  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient ) { }

  public getRiders() : Observable<Rider[]> {
    return this.http.get<Rider[]>(`${this.apiServerUrl}/riders/`);
  }

  public addRider(rider : Rider) : Observable<Rider> {
    return this.http.post<Rider>(`${this.apiServerUrl}/riders/`, rider);
  }

  public updateRider(rider : Rider) : Observable<Rider> {
    return this.http.put<Rider>(`${this.apiServerUrl}/riders/`, rider);
  }

  public deleteRider(riderId : number) : Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/riders/${riderId}`);
  }


}
